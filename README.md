stackengine-cluster
============================



## Description
stackengine-cluster


## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/stackengine-cluster/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

### `STACKENGINE_CLIENT_ID`:
  * description: the client id from your StackEngine account


## Required variables with default

### `CLUSTER_ELB_TRAFFIC_PORTS`:
  * description: ports that need to allow traffic into the ELB
  * default: 50, 1f41, 1f42, 1093

### `CLUSTER_INSTANCE_TRAFFIC_PORTS`:
  * description: ports to allow traffic on directly to the instances
  * default: 20fb, 1f41, 1f42, 1093, 16

### `STACKENGINE_UPSTREAM_DNS`:
  * description: the dns entry of the load balancer in front of the stack
  * default: COMPOSITE::coreo_aws_ec2_elb.${CLUSTER_NAME}-elb.dns_name



## Optional variables with default

### `ELB_LISTENERS`:
  * description: The listeners to apply to the ELB
  * default: 
```
[  
  {    
    :elb_protocol => 'http',    :elb_port => 80,
    :to_protocol => 'https',    :to_port => 8443
  },
  {    
    :elb_protocol => 'tcp',    :elb_port => 8001,
    :to_protocol => 'tcp',     :to_port => 8001
  },
  {
    :elb_protocol => 'tcp',    :elb_port => 8002,
    :to_protocol => 'tcp',     :to_port => 8002  
  }
]

```


## Optional variables with no default

**None**

## Tags
1. Servers
1. Stackengine


## Categories
1. Servers



## Diagram
![diagram](https://raw.githubusercontent.com/CloudCoreo/stackengine-cluster/master/images/diagram.png "diagram")


## Icon
![icon](https://raw.githubusercontent.com/CloudCoreo/stackengine-cluster/master/images/icon.png "icon")

