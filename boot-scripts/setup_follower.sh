#!/bin/bash

stackengine_work_dir='/var/tmp/stackengine'
client_id="${STACKENGINE_CLIENT_ID}"

mkdir -p "$stackengine_work_dir"

# go get the install script
cd "$stackengine_work_dir"
curl -s "https://app.stackengine.com/install/$client_id/linux" -o seinstall.sh

## backgrounding it now to let the rest continue
nohup bash seinstall.sh -leaderip="$STACKENGINE_UPSTREAM_DNS" -follower &
