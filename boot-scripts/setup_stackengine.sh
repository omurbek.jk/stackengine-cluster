#!/bin/bash
######################################################################
##
## Required Variables:
##   STACKENGINE_CLIENT_ID
##   STACKENGINE_UPSTREAM_DNS
######################################################################
set -x

source /etc/profile.d/cluster

if [ "true" = "$IS_LEADER" ]; then
    ## install the leader way
    (
	bash setup_leader.sh
    )
else
    ## install the follower way
    (
	bash setup_follower.sh
    )
fi

